from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import time

pesquisa = input("Digite a pesquisa: ")
# pesquisa = "Teste"

s=Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=s)
driver.get("https://www.google.com")

campo = driver.find_element_by_xpath("//input[@aria-label='Pesquisar']")
campo.send_keys(pesquisa)
campo.send_keys(Keys.ENTER)
time.sleep(1)

resultados = driver.find_element_by_xpath("//div[@id='result-stats']").text
print(resultados)

numero_resultados = int(resultados.split("Aproximadamente ")[1].split(" resultados")[0].replace(".", ""))
numero_paginas = numero_resultados/10

num_paginas = int(input("Numero de páginas: %s, até qual página iremos?" % (numero_paginas)))

url_pagina = driver.find_element_by_xpath("//a[@aria-label='Page 2']").get_attribute("href")

pagina_atual = 0
start = 10
lista_resultados = []
while pagina_atual < num_paginas:
  if pagina_atual > 1:
    url_pagina = url_pagina.replace("start=%s" % start, "start=%s" % (start+10))
    start += 10
    driver.get(url_pagina)
  elif pagina_atual == 1:
    driver.get(url_pagina)
  pagina_atual += 1

  divs = driver.find_elements_by_xpath("//div[@class='g']")
  for div in divs:
    nome = div.find_element_by_tag_name("span").text
    link = div.find_element_by_tag_name("a").get_attribute("href")
    resultado = "%s;%s" % (nome,link)
    print(resultado)
    lista_resultados.append(resultado)

with open("resultados.txt", "w") as arquivo:
  for resultado in lista_resultados:
    arquivo.write("%s\n" % resultado)
  arquivo.close()

print("%s resultados encontrados no Google" % len(lista_resultados))

time.sleep(4)
