from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import time
import xlrd

print("Iniciando o robo \n")

arq = open("resultado.txt", "w")

print("Lendo do excel")
dominios = []
workbook = xlrd.open_workbook("dominio.xlsx")
sheet = workbook.sheet_by_index(0)
# print(sheet.cell_value(0,0))

num_lines = 4

for linha in range(0,num_lines):
  print(linha)
  cell_obj = sheet.cell(linha, 0)
  if cell_obj is not xlrd.empty_cell:
    dominios.append(sheet.cell_value(linha,0))

s=Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=s)
# driver = webdriver.Chrome("/home/diego/Projetos/Python/robos//home/diego/Projetos/Python/robos")
driver.get("https://registro.br/")

# dominios = ["roboscompython.com.br", "udemy.com", "uol.com.br", "pythoncurso.com"]

for dominio in dominios:
  pesquisa = driver.find_element_by_id("is-avail-field")
  pesquisa.clear()
  pesquisa.send_keys(dominio)
  pesquisa.send_keys(Keys.RETURN)

  time.sleep(1)

  resultados = driver.find_elements_by_tag_name("strong")
  texto = "Domínio %s %s \n" % (dominio, resultados[4].text)
  arq.write(texto)
# import pdb; pdb.set_trace()

arq.close()
time.sleep(4)
driver.close()